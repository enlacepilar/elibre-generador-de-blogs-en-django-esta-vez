# set the base image 
FROM python:3.10

# File Author / Maintainer
MAINTAINER Hugue

#add project files to the usr/src/app folder
ADD . /usr/src/app

#set directoty where CMD will execute 
WORKDIR /usr/src/app

COPY requerimientos.txt ./

# Get pip to download and install requirements:
RUN pip install --no-cache-dir -r requerimientos.txt

CMD ["gunicorn", "traducciones.wsgi:application", "--bind", "0.0.0.0:5001", "--workers", "3"]

EXPOSE 5001

