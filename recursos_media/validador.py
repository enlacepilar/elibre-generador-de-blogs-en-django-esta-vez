import os
from django.core.exceptions import ValidationError
from PIL import Image


def valida_PDF(value):
    
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Sólo archivos en PDF.')

def valida_IMAGEN(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.png', '.jpeg', '.gif', '.webp']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Las tapas sólo pueden subirse en JPG, JPEG, PNG, WEBP o un GIF')
    #convertir imagen luego de subida
        
    # Reducir la imagen después de subirla
    imagen1 = Image.open(value)
    imagen1.thumbnail((800, 675), Image.LANCZOS)

    # Guardar la imagen reducida en la ubicación deseada
    #ruta_archivo = "ruta/donde/guardar/la/imagen.jpg"  # Reemplaza con la ruta deseada
    imagen1.save(value)

def limite_PDF(value): 
    limite = 50 * 1024 * 1024
    if value.size > limite:
        raise ValidationError('El archivo es muy pesadooo. Tengo una Raspberry amigo, no estamos en lo servidores de AWS o Guguel, no seas tan golos@ X)')

def limite_IMAGEN(value): 
    limite = 5 * 1024 * 1024
    if value.size > limite:
        raise ValidationError('La imagen es muy pesada. ¿Se podrá reducir?)')