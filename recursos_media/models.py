from django.db import models

from django.conf import settings
from django.utils import timezone

from .validador import *

class Categorias (models.Model):
    nombre = models.CharField(max_length=200, verbose_name="Nombre categoria")
    fecha_creacion = models.DateTimeField(
        default=timezone.now
    )
    fecha_modificacion = models.DateTimeField(
        blank=True, null=True
    )

    
    def __str__(self):
        #return self.titulo
        return self.nombre

class Publicaciones (models.Model):
    titulo = models.CharField(max_length=200)
    autor_id = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, on_delete=models.DO_NOTHING)
    categoria_id = models.ForeignKey(Categorias, null=True, on_delete=models.DO_NOTHING)
    imagen = models.ImageField(upload_to="imagenes_publicaciones", null=False, blank=True, verbose_name='Imagen', validators=[valida_IMAGEN, limite_IMAGEN])
    resumen = models.CharField(max_length=255)
    texto = models.TextField(null=False, verbose_name="Texto del contenido")
    fecha_creacion = models.DateTimeField(
        default=timezone.now
    )
    fecha_modificacion = models.DateTimeField(
        blank=True, null=True
    )

    def publicar (self):
        self.fecha_publicacion = timezone.now()
        self.save()

    def __str__(self):
        return self.titulo
        #return self.nombre


class Presentacion (models.Model):
    titulo = models.CharField(max_length=200)
    autor_id = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, on_delete=models.DO_NOTHING)
    texto_presentacion = models.TextField(null=False, verbose_name="Texto presentacion")
    fecha_creacion = models.DateTimeField(
        default=timezone.now
    )
    fecha_modificacion = models.DateTimeField(
        blank=True, null=True
    )

    def publicar (self):
        self.fecha_publicacion = timezone.now()
        self.save()

    def __str__(self):
        return self.texto_presentacion
        #return self.nombre