from django.apps import AppConfig


class RecursosMediaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'recursos_media'
