from django.shortcuts import render

from django.views.generic import ListView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)
#from django.core.urlresolvers import reverse_lazy #Version vieja
from django.urls import reverse_lazy
from django.contrib import messages

from django.shortcuts import redirect

from django.contrib.auth.mixins import LoginRequiredMixin

from recursos_media.models import Categorias


class CategoriasListar (LoginRequiredMixin,ListView):
    model = Categorias
    template_name = 'listado-categorias.html'
    paginate_by = 10
    context_object_name = 'lista categorias'
    ordering = ['-fecha_creacion']

class CategoriaCargar(LoginRequiredMixin,CreateView):
    model = Categorias
    success_url = reverse_lazy('lista categorias')
    template_name = 'cargar-categoria.html'
    fields = ['nombre']
    context_object_name = 'carga nueva categoria'

    def form_valid(self, form):
      messages.success(self.request, "Nueva categoría cargada.")
      super().form_valid(form)
      #return HttpResponseRedirect(self.get_success_url())
      return redirect ('lista categorias')

class CategoriaActualizar(LoginRequiredMixin,UpdateView):
    model = Categorias
    template_name = 'editar-categoria.html'
    fields = ['nombre']
    context_object_name = 'actualizar categoria'
    success_url = reverse_lazy('lista categorias')
    
    def form_valid(self, form):
      messages.success(self.request, "¡La categoría fue actualizada!")
      super().form_valid(form)
      #return HttpResponseRedirect(self.get_success_url())
      return redirect ('lista categorias')
    
class CategoriaBorrar(LoginRequiredMixin, DeleteView):
    model = Categorias
    template_name = 'confirma-borrado.html'
    success_url = reverse_lazy('lista categorias')
    context_object_name = 'borra una categoria'

    def form_valid(self, form):
        messages.success(self.request, "¡La info fue borrada!")
        super().form_valid(form)
        #return HttpResponseRedirect(self.get_success_url())
        return redirect ('lista categorias')
