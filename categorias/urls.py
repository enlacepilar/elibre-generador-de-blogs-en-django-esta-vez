from django.urls import path
from . import views
from .views import CategoriasListar, CategoriaCargar, CategoriaActualizar, CategoriaBorrar

urlpatterns = [
    path('', CategoriasListar.as_view(), name='lista categorias'),
    path('cargar/', CategoriaCargar.as_view(), name='carga nueva categoria'),
    path('actualizar/<int:pk>', CategoriaActualizar.as_view(), name='actualizar categoria'),
    path('borrar/<int:pk>', CategoriaBorrar.as_view(), name='borra una categoria'),   

]
