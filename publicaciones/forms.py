from django import forms
from django.forms import ModelForm, Select
from recursos_media.models import Publicaciones

class PublicacionesForm(forms.ModelForm):
    #texto = forms.TextareaField()

    class Meta:
        model = Publicaciones
        fields = ['titulo', 'categoria_id', 'resumen', 'texto', 'imagen']
        widgets = {
            #'sexo_id': Select(attrs={'class': 'form-control'}, required=False),
            'categoria_id': Select(attrs={'class': 'form-control'}),
            'texto': forms.Textarea(attrs={'class': 'form-control'}),
         
        }

        labels = {
            'categoria_id': 'Categoría',
            'titulo': 'Titulo de la Publiacción',
            'resumen': 'Breve Resumen:',
            'texto': 'El texto de la publicación',
        }

        help_texts = {
            'titulo': 'Ingresar el título de la publicación',
            'categoría_id': 'Seleccionar categoría',
            'resumen': 'Escribir un breve resumen',
            'texto': 'Escribir...',
          
        }

        error_messages = {
            'titulo': {
                'required': 'Este campo es obligatorio',
            },
            'resumen': {
                'required': 'Este campo es obligatorio',
            },
            'texto': {
                'required': 'Este campo es obligatorio',
            },
            'imagen': {
                'required': 'Este campo es obligatorio',
            },
        }

imagen = forms.ImageField(required=False)

    # def __init__(self, *args, **kwargs):
    #     super(PublicacionesForm, self).__init__(*args, **kwargs)
    #     self.fields['fecha_modificacion'].required = False
      

