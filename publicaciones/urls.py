from django.urls import path
from . import views
from .views import PublicacionesListar, TablaEntradas, PublicacionDetalle, PublicacionCargar, PublicacionActualizar, PublicacionBorrar 




urlpatterns = [
    path('', PublicacionesListar.as_view(), name='lista publicaciones'),
    path('tabla_entradas', TablaEntradas.as_view(), name='tabla de entradas'),
    path('detalle/<int:pk>', PublicacionDetalle.as_view(), name='publicacion'),
    path('cargar/', PublicacionCargar.as_view(), name='carga nueva publicacion'),
    path('actualizar/<int:pk>', PublicacionActualizar.as_view(), name='actualizar publicacion'),
    path('borrar/<int:pk>', PublicacionBorrar.as_view(), name='borra una publicacion'),   

]
