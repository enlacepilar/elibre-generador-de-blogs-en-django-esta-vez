from django.shortcuts import render

from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)

from django.urls import reverse_lazy
from django.contrib import messages

from django.shortcuts import redirect

from django.contrib.auth.mixins import LoginRequiredMixin

from recursos_media.models import Publicaciones, Categorias
from .forms import PublicacionesForm
from django.http import HttpResponse

class PublicacionesListar (ListView):
    model = Publicaciones
    template_name = 'inicio-blog.html'
    paginate_by = 10
    context_object_name = 'lista publicaciones'
    ordering = ['-fecha_creacion']  # Ordenar por fecha de publicación descendente

class TablaEntradas (ListView):
    model = Publicaciones
    template_name = 'tabla-entradas.html'
    paginate_by = 10
    context_object_name = 'tabla de entradas'
    ordering = ['-fecha_creacion']

class PublicacionDetalle(DetailView):
    model = Publicaciones
    template_name = 'detalle-publicacion.html'
    context_object_name = 'publicacion'

class PublicacionCargar(LoginRequiredMixin,CreateView):
    model = Publicaciones
    form_class = PublicacionesForm
    success_url = reverse_lazy('lista publicaciones')
    template_name = 'cargar-publicacion.html'
    #fields = ['titulo', 'categoria_id', 'imagen', 'resumen', 'texto']
    context_object_name = 'carga nueva publicacion'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["listo_categoria"] = Categorias.objects.all()
        return context
    
    def form_valid(self, form):
        form.instance.autor_id = self.request.user  # Asigna el usuario registrado como autor
        if not form.instance.imagen:
            form.instance.imagen = 'imagenes_publicaciones/sin-imagen.webp'
            
        messages.success(self.request, "¡Nueva entrada cargada!")
        return super().form_valid(form)

class PublicacionActualizar(LoginRequiredMixin,UpdateView):
    model = Publicaciones
    template_name = 'editar-publicacion.html'
    fields = ['titulo', 'categoria_id', 'resumen', 'texto']
    context_object_name = 'actualizar publicacion'
    success_url = reverse_lazy('tabla de entradas')
    
    def form_valid(self, form):
      messages.success(self.request, "¡La info fue actualizada!")
      super().form_valid(form)
      #return HttpResponseRedirect(self.get_success_url())
      return redirect ('tabla de entradas')
    
class PublicacionBorrar(DeleteView):
    model = Publicaciones
    template_name = 'confirma-borrado.html'
    success_url = reverse_lazy('tabla de entradas')
    context_object_name = 'borra una publicacion'

    def form_valid(self, form):
      messages.success(self.request, "¡La publicación fue eliminada!")
      super().form_valid(form)
      #return HttpResponseRedirect(self.get_success_url())
      return redirect ('tabla de entradas')