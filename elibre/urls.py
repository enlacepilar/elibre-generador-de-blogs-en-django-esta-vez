from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('publicaciones.urls')),
    path('categorias/', include('categorias.urls')),
    path('presentacion/', include('presentacion.urls')),
    path('recursos_media/', include('recursos_media.urls')),
    path("ingreso/", include("django.contrib.auth.urls")), #Esto es para el logueo
    path("datos_usuario/", include("ingreso.urls")), #Esto es para el logueo
]

#Esto sirve para ver la url de las fotos en modo debug, si no vas a renegar y no se ven
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)