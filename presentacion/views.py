from django.shortcuts import render

from django.views.generic import ListView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)
#from django.core.urlresolvers import reverse_lazy #Version vieja
from django.urls import reverse_lazy
from django.contrib import messages

from django.shortcuts import redirect

from django.contrib.auth.mixins import LoginRequiredMixin

from recursos_media.models import Presentacion


class PresentacionesListar (ListView):
    model = Presentacion
    template_name = 'muestra-presentaciones.html'
    paginate_by = 10
    context_object_name = 'muestra presentacion'
    ordering = ['-fecha_creacion']

class PresentacionCargar(LoginRequiredMixin,CreateView):
    model = Presentacion
    success_url = reverse_lazy('muestra presentacion')
    template_name = 'cargar-presentacion.html'
    fields = ['titulo', 'texto_presentacion']
    context_object_name = 'carga nueva presentacion'

    def form_valid(self, form):
        form.instance.autor_id = self.request.user  # Asigna el usuario registrado como autor
        messages.success(self.request, "Nueva presentación cargada.")
        super().form_valid(form)
        #return HttpResponseRedirect(self.get_success_url())
        return redirect ('muestra presentacion')

class PresentacionActualizar(LoginRequiredMixin,UpdateView):
    model = Presentacion
    template_name = 'editar-presentacion.html'
    fields = ['titulo', 'texto_presentacion']
    context_object_name = 'actualizar presentacion'
    success_url = reverse_lazy('muestra presentacion')
    
    def form_valid(self, form):
      messages.success(self.request, "¡La presentación fue actualizada!")
      super().form_valid(form)
      #return HttpResponseRedirect(self.get_success_url())
      return redirect ('muestra presentacion')
    
class PresentacionBorrar(LoginRequiredMixin, DeleteView):
    model = Presentacion
    template_name = 'confirma-borrado.html'
    success_url = reverse_lazy('muestra presentacion')
    context_object_name = 'borra una presentacion'

    def form_valid(self, form):
        messages.success(self.request, "¡La info fue borrada!")
        super().form_valid(form)
        #return HttpResponseRedirect(self.get_success_url())
        return redirect ('muestra presentacion')
