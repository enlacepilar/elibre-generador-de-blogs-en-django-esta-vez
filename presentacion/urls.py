from django.urls import path
from . import views
from .views import PresentacionesListar, PresentacionCargar, PresentacionActualizar, PresentacionBorrar

urlpatterns = [
    path('', PresentacionesListar.as_view(), name='muestra presentacion'),
    path('cargar/', PresentacionCargar.as_view(), name='carga nueva presentacion'),
    path('actualizar/<int:pk>', PresentacionActualizar.as_view(), name='actualizar presentacion'),
    path('borrar/<int:pk>', PresentacionBorrar.as_view(), name='borra una presentacion'),   

]
