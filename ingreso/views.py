from django.shortcuts import render, redirect
from django.contrib import messages

from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin


class ActualizarDatos(LoginRequiredMixin, UpdateView):
    model = User  # El modelo que deseas actualizar (en este caso, User)
    template_name = 'actualizar-datos.html'  # La plantilla HTML para el formulario de actualización
    fields = ['username', 'email']  # Los campos que se pueden actualizar
    success_url = reverse_lazy('lista publicaciones')

def cierraSesion (request):
    context_object_name = "cerrar sesion"
    request.session.clear()
    request.session.flush()
    messages.success(request, "Sesión cerrada correctamente.")
    return redirect ('lista publicaciones')
