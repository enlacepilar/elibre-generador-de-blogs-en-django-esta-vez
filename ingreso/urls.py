from django.urls import path
from . import views
from .views import ActualizarDatos


urlpatterns = [
    
    path('cierraSesion', views.cierraSesion, name='cerrar sesion usuario'),
    path('actualizaDatos/<int:pk>/', ActualizarDatos.as_view(), name='actualizar mis datos'),    
]
